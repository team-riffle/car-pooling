package com.teamriffle.carpooling.entities

import kotlinx.serialization.Serializable

@Serializable
data class User(
    val fullName: String?,
    val nickname: String?,
    val email: String?,
    val location: String?,
    val avatarUri: String?,)