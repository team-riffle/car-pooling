package com.teamriffle.carpooling.entities

class RequestCode {
    companion object{
        val SAVE_PROFILE = 1
        val CAMERA_PROFILE = 2
        val GALLERY_PROFILE = 3
        val RC_SIGN_IN = 4
    }
}