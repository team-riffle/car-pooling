package com.teamriffle.carpooling.entities

data class Booked(
    val id: String?,
    val nickname: String?,
    val status: Boolean
)