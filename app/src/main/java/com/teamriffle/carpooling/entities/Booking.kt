package com.teamriffle.carpooling.entities

import kotlinx.serialization.Serializable

@Serializable
data class Booking(
    val id: Int?,
    val rider: String?,
    val date: String?,
    val time: String?,
    val location: String?,
    val amount: Int?,
    val cell: Int?,
    val paymentStatus: String?
)