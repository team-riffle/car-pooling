package com.teamriffle.carpooling.entities

import android.net.Uri
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.ImageView
import android.widget.TextView
import androidx.cardview.widget.CardView
import androidx.core.os.bundleOf
import androidx.navigation.findNavController
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.google.firebase.storage.FirebaseStorage
import com.teamriffle.carpooling.R
import kotlinx.serialization.Serializable


enum class TripStatus{
    AVAILABLE,
    FULL,
    BLOCKED
}

@Serializable
data class Trip(
        val id: Int?,
        val departure: String?,
        val arrival: String?,
        val date: String?,
        val duration: String?,
        val seats: String?,
        val price: String?,
        val desc: String? = "",
        val intermediateStops: String? = "",
        val carUri: String? = "",
        val other: Boolean = true,
        val rider: String?,
        val status: TripStatus
)

class TripAdapter(var trips_: List<Trip>): RecyclerView.Adapter<TripAdapter.TripViewHolder>(){
    class TripViewHolder(v: View): RecyclerView.ViewHolder(v){
        val card = v.findViewById<CardView>(R.id.tripCard)
        val image = v.findViewById<ImageView>(R.id.imageUri)
        val departure = v.findViewById<TextView>(R.id.from)
        val arrival = v.findViewById<TextView>(R.id.to)
        val date = v.findViewById<TextView>(R.id.date)
        val duration = v.findViewById<TextView>(R.id.duration)
        val price = v.findViewById<TextView>(R.id.price)
        val seats = v.findViewById<TextView>(R.id.seats)
        val edit = v.findViewById<Button>(R.id.edit)
        fun bind(trip: Trip){
            if(trip.other)
                card.setOnClickListener {
                    it.findNavController().navigate(R.id.action_nav_othersTripList_to_nav_tripDetails, bundleOf(
                            "com.teamriffle.carpooling.ACTION" to "View",
                            "com.teamriffle.carpooling.ID" to trip.id,
                            "com.teamriffle.carpooling.DEPARTURE" to trip.departure,
                            "com.teamriffle.carpooling.ARRIVAL" to trip.arrival,
                            "com.teamriffle.carpooling.DATE" to trip.date,
                            "com.teamriffle.carpooling.SEATS" to trip.seats,
                            "com.teamriffle.carpooling.PRICE" to trip.price,
                            "com.teamriffle.carpooling.DESC" to trip.desc,
                            "com.teamriffle.carpooling.CAR_URI" to trip.carUri,
                            "com.teamriffle.carpooling.DURATION" to trip.duration,
                            "com.teamriffle.carpooling.INTERMEDIATE_STOPS" to trip.intermediateStops,
                            "com.teamriffle.carpooling.RIDER" to trip.rider,
                            "com.teamriffle.carpooling.STATUS" to trip.status.toString()
                    ))
                }
            else
                card.setOnClickListener {
                    it.findNavController().navigate(R.id.action_nav_tripList_to_nav_tripDetails, bundleOf(
                            "com.teamriffle.carpooling.ACTION" to "Edit",
                            "com.teamriffle.carpooling.ID" to trip.id,
                            "com.teamriffle.carpooling.DEPARTURE" to trip.departure,
                            "com.teamriffle.carpooling.ARRIVAL" to trip.arrival,
                            "com.teamriffle.carpooling.DATE" to trip.date,
                            "com.teamriffle.carpooling.SEATS" to trip.seats,
                            "com.teamriffle.carpooling.PRICE" to trip.price,
                            "com.teamriffle.carpooling.DESC" to trip.desc,
                            "com.teamriffle.carpooling.CAR_URI" to trip.carUri,
                            "com.teamriffle.carpooling.DURATION" to trip.duration,
                            "com.teamriffle.carpooling.INTERMEDIATE_STOPS" to trip.intermediateStops,
                            "com.teamriffle.carpooling.RIDER" to trip.rider,
                            "com.teamriffle.carpooling.STATUS" to trip.status.toString()
                    ))
                }

            val imgRef = FirebaseStorage.getInstance().getReference("/tripsImgs/${trip.rider}/${trip.id}")
            imgRef.downloadUrl.addOnSuccessListener { url ->
                try {
                    Glide.with(itemView)
                            .load(url)
                            .into(image)
                }catch (e: Exception){ }
            }
            //image.setImageURI(Uri.parse(trip.carUri))
            departure.text = trip.departure
            arrival.text = trip.arrival
            date.text = trip.date
            duration.text = trip.duration
            price.text = trip.price
            seats.text = trip.seats
            if(trip.other)
                edit.visibility = View.INVISIBLE
            else
                edit.setOnClickListener { it.findNavController().navigate(R.id.action_nav_tripList_to_nav_tripEdit,bundleOf(
                        "com.teamriffle.carpooling.ACTION" to "Edit",
                        "com.teamriffle.carpooling.ID" to trip.id,
                        "com.teamriffle.carpooling.DEPARTURE" to trip.departure,
                        "com.teamriffle.carpooling.ARRIVAL" to trip.arrival,
                        "com.teamriffle.carpooling.DATE" to trip.date,
                        "com.teamriffle.carpooling.SEATS" to trip.seats,
                        "com.teamriffle.carpooling.PRICE" to trip.price,
                        "com.teamriffle.carpooling.DESC" to trip.desc,
                        "com.teamriffle.carpooling.CAR_URI" to trip.carUri,
                        "com.teamriffle.carpooling.DURATION" to trip.duration,
                        "com.teamriffle.carpooling.INTERMEDIATE_STOPS" to trip.intermediateStops,
                        "com.teamriffle.carpooling.RIDER" to trip.rider,
                        "com.teamriffle.carpooling.STATUS" to trip.status.toString()
                ))
                }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): TripViewHolder {
        val layout = LayoutInflater.from(parent.context)
                .inflate(R.layout.trip_card_layout, parent,false)
        return TripViewHolder(layout)
    }

    override fun onBindViewHolder(holder: TripViewHolder, position: Int) {
        holder.bind(trips_[position])
    }

    override fun getItemCount(): Int {
        return trips_.size
    }

    fun setTrips(newTrips: List<Trip>){
        val diffs = DiffUtil.calculateDiff(
                TripDiffCallback(trips_, newTrips)
        )
        trips_ = newTrips
        diffs.dispatchUpdatesTo(this)
    }

    class TripDiffCallback(private val oldTrips: List<Trip>,
                           private val newTrips: List<Trip>): DiffUtil.Callback() {

        override fun getOldListSize(): Int = oldTrips.size
        override fun getNewListSize(): Int = newTrips.size
        override fun areItemsTheSame(oldP: Int, newP: Int): Boolean{
            return oldTrips[oldP].id == newTrips[newP].id}
        override fun areContentsTheSame(oldPosition: Int, newPosition: Int): Boolean{
            val(departure, arrival) = oldTrips[oldPosition]
            val(departure1, arrival1) = newTrips[newPosition]
            return  departure == departure1 && arrival == arrival1
        }
    }
}