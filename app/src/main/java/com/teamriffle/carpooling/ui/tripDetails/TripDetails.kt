package com.teamriffle.carpooling.ui.tripDetails

import android.content.Context
import android.net.Uri
import android.opengl.Visibility
import android.os.Build
import android.os.Bundle
import android.util.Log
import android.view.*
import android.view.View.INVISIBLE
import android.view.View.VISIBLE
import android.widget.*
import androidx.annotation.RequiresApi
import androidx.core.os.bundleOf
import androidx.fragment.app.Fragment
import androidx.fragment.app.setFragmentResultListener
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.findNavController
import com.bumptech.glide.Glide
import com.google.android.material.floatingactionbutton.ExtendedFloatingActionButton
import com.google.android.material.floatingactionbutton.FloatingActionButton
import com.google.android.material.snackbar.Snackbar
import com.google.firebase.firestore.FieldValue
import com.google.firebase.firestore.FirebaseFirestore
import com.google.firebase.firestore.ListenerRegistration
import com.google.firebase.storage.FirebaseStorage
import com.teamriffle.carpooling.MainActivity
import com.teamriffle.carpooling.R
import com.teamriffle.carpooling.entities.TripStatus


class TripDetails : Fragment() {

    companion object {
        fun newInstance() = TripDetails()
    }

    private lateinit var listener: ListenerRegistration
    private lateinit var viewModel: TripDetailsViewModel

    private lateinit var action: String
    private lateinit var rider: String
    private lateinit var statusV: TripStatus
    private lateinit var booked: MutableMap<String,Boolean>
    private lateinit var trip_id: Number
    private lateinit var departureV: TextView
    private lateinit var arrivalV: TextView
    private lateinit var dateV: TextView
    private lateinit var durationV: TextView
    private lateinit var seatsV: TextView
    private lateinit var priceV: TextView
    private lateinit var descV: TextView
    private lateinit var intermediateStopsV: TextView
    private lateinit var carPhotoV: ImageView
    private lateinit var carUriV: Uri
    private lateinit var myContext: Context


    @RequiresApi(Build.VERSION_CODES.KITKAT)
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {

        if (container != null) {
            myContext = container.context
        }
        return inflater.inflate(R.layout.fragment_trip_details, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        departureV = view.findViewById(R.id.tripDetails_departure_value)
        arrivalV = view.findViewById(R.id.tripDetails_arrival_value)
        dateV = view.findViewById(R.id.tripDetails_date_value)
        durationV = view.findViewById(R.id.tripDetails_duration_value)
        seatsV = view.findViewById(R.id.tripDetails_seats_value)
        priceV = view.findViewById(R.id.tripDetails_price_value)
        descV = view.findViewById(R.id.tripDetails_desc_value)
        intermediateStopsV = view.findViewById(R.id.tripDetails_intermediateStops_value)
        carPhotoV = view.findViewById(R.id.imageView6)
        val fab: ExtendedFloatingActionButton = view.findViewById(R.id.fab)
        val fab2: ExtendedFloatingActionButton = view.findViewById(R.id.fab2)

        action = arguments?.getString("com.teamriffle.carpooling.ACTION").toString()
        rider = arguments?.getString("com.teamriffle.carpooling.RIDER").toString()
        trip_id = arguments?.getInt("com.teamriffle.carpooling.ID")!!
        statusV = TripStatus.valueOf(arguments?.getString("com.teamriffle.carpooling.STATUS").toString())

        if(statusV != TripStatus.BLOCKED) fab2.visibility = INVISIBLE

        departureV.setText(arguments?.getString("com.teamriffle.carpooling.DEPARTURE"))
        arrivalV.setText(arguments?.getString("com.teamriffle.carpooling.ARRIVAL"))
        dateV.setText(arguments?.getString("com.teamriffle.carpooling.DATE"))
        priceV.setText(arguments?.getString("com.teamriffle.carpooling.PRICE"))
        seatsV.setText(arguments?.getString("com.teamriffle.carpooling.SEATS"))
        descV.setText(arguments?.getString("com.teamriffle.carpooling.DESC"))
        carUriV = Uri.parse(arguments?.getString("com.teamriffle.carpooling.CAR_URI"))
        durationV.setText(arguments?.getString("com.teamriffle.carpooling.DURATION"))
        intermediateStopsV.setText(arguments?.getString("com.teamriffle.carpooling.INTERMEDIATE_STOPS"))

        val db: FirebaseFirestore = FirebaseFirestore.getInstance()
        val tripsRef = db.collection("users/$rider/trips")
        val tripRef = tripsRef.document("$trip_id")
        val imgRef = FirebaseStorage.getInstance().getReference("/tripsImgs/$rider/$trip_id")
        val userId = (activity as MainActivity).getEmail()

        listener = tripRef.addSnapshotListener{ value, error ->
            if(error != null) throw error
            if(value != null){
                if(value["bookedMap"] != null)
                    booked = value["bookedMap"] as MutableMap<String,Boolean>
                else
                    booked = mutableMapOf()

                statusV = TripStatus.valueOf(value["status"].toString())
                if(statusV != TripStatus.BLOCKED) fab2.visibility = INVISIBLE
                else fab2.visibility = VISIBLE

                if(action == "View") {
                    if (statusV == TripStatus.BLOCKED)
                        fab.text = "Blocked"
                    else {
                        if (booked.keys.contains(userId))
                            fab.text = "Cancel"
                        else
                            fab.text = "Book"
                    }
                    fab2.visibility = INVISIBLE
                }

//                if(action == "Edit" && status == TripStatus.AVAILABLE) fab2.visibility = INVISIBLE


                departureV.setText(value["departure"].toString())
                arrivalV.setText(value["arrival"].toString())
                dateV.setText(value["date"].toString())
                priceV.setText(value["price"].toString())
                seatsV.setText(value["seats"].toString())
                descV.setText(value["desc"].toString())
                durationV.setText(value["duration"].toString())
                intermediateStopsV.setText(value["intermediateStops"].toString())

                imgRef.downloadUrl.addOnSuccessListener { url ->
                    Glide.with(myContext)
                            .load(url)
                            .into(carPhotoV)
                }
            }
        }

        setFragmentResultListener("requestKey") { _, bundle ->
            action = bundle.getString("com.teamriffle.carpooling.ACTION").toString()

            val id = bundle.getInt("com.teamriffle.carpooling.ID")
            val departure = bundle.getString("com.teamriffle.carpooling.DEPARTURE")
            val arrival = bundle.getString("com.teamriffle.carpooling.ARRIVAL")
            val date = bundle.getString("com.teamriffle.carpooling.DATE")
            val duration = bundle.getString("com.teamriffle.carpooling.DURATION")
            val seats = bundle.getString("com.teamriffle.carpooling.SEATS")
            val price = bundle.getString("com.teamriffle.carpooling.PRICE")
            val desc = bundle.getString("com.teamriffle.carpooling.DESC")
            val intermediateStops = bundle.getString("com.teamriffle.carpooling.INTERMEDIATE_STOPS")
            val carUri = bundle.getString("com.teamriffle.carpooling.CAR_URI")
            val status = bundle.getString("com.teamriffle.carpooling.STATUS").toString()

            val newPhoto = bundle.getBoolean( "com.teamriffle.carpooling.NEWPHOTO")

            if(newPhoto) {
                imgRef.putFile(Uri.parse(carUri)).addOnSuccessListener {
                    tripRef.update(mapOf(
                            "id" to id,
                            "departure" to departure,
                            "arrival" to arrival,
                            "date" to date,
                            "duration" to duration,
                            "seats" to seats,
                            "price" to price,
                            "desc" to desc,
                            "intermediateStops" to intermediateStops,
                            "carUri" to carUri,
                            "status" to status
                    ))
                    carPhotoV.setImageURI(Uri.parse(carUri))
                }
            } else{
                tripRef.update(mapOf(
                        "id" to id,
                        "departure" to departure,
                        "arrival" to arrival,
                        "date" to date,
                        "duration" to duration,
                        "seats" to seats,
                        "price" to price,
                        "desc" to desc,
                        "intermediateStops" to intermediateStops,
                        "carUri" to carUri,
                        "status" to status
                ))
            }

            trip_id = id
            departureV.setText(departure)
            arrivalV.setText(arrival)
            dateV.setText(date)
            durationV.setText(duration)
            priceV.setText(price)
            seatsV.setText(seats)
            descV.setText(desc)
            carUriV = Uri.parse(carUri)
            intermediateStopsV.setText(intermediateStops)
            statusV = TripStatus.valueOf(status)

            if(statusV != TripStatus.BLOCKED) fab2.visibility = INVISIBLE
            else fab2.visibility = VISIBLE
        }

        if(action == "Edit"){
            fab.text = "Booked"
            fab2.text = "Blocked"
            fab.setOnClickListener{v ->
                view.findNavController()
                    .navigate(R.id.action_nav_tripDetails_to_nav_tripBooked, bundleOf(
                        "com.teamriffle.carpooling.ID" to trip_id
                    ))
            }
        }else{
            if(statusV != TripStatus.BLOCKED)
            fab.setOnClickListener{v ->
                    if(booked.keys.contains(userId)) { //TODO: usare lo userId del login
                        //Cancella booking
                        booked.remove(userId)
                        tripRef.update("bookedMap", booked)
                    }else{
                        if(seatsV.text.toString().toInt() > 0) {
                            //Aggiungi booking
                            booked[userId] = false
                            tripRef.update("bookedMap", booked)
                        }else {
                            view.let {
                                Snackbar.make(it, "No seats available", Snackbar.LENGTH_LONG)
                                        .setAction("Action", null).show()
                            }
                        }
                }
            }
        }
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        viewModel = ViewModelProvider(this).get(TripDetailsViewModel::class.java)
        // TODO: Use the ViewModel
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setHasOptionsMenu(true)
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        super.onCreateOptionsMenu(menu, inflater)
        menu.clear()

        inflater.inflate(R.menu.profile_menu, menu)

        if(action == "View"){
            val item = menu.findItem(R.id.edit_profile_menu_item)
            item?.setVisible(false)
        }
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            R.id.edit_profile_menu_item -> {
                view?.findNavController()?.navigate(R.id.action_nav_tripDetails_to_nav_tripEdit, bundleOf(
                        "com.teamriffle.carpooling.ACTION" to "Edit",
                        "com.teamriffle.carpooling.ID" to trip_id,
                        "com.teamriffle.carpooling.DEPARTURE" to departureV.text,
                        "com.teamriffle.carpooling.ARRIVAL" to arrivalV.text,
                        "com.teamriffle.carpooling.DATE" to dateV.text,
                        "com.teamriffle.carpooling.SEATS" to seatsV.text,
                        "com.teamriffle.carpooling.PRICE" to priceV.text,
                        "com.teamriffle.carpooling.DESC" to descV.text,
                        "com.teamriffle.carpooling.CAR_URI" to carUriV.toString(),
                        "com.teamriffle.carpooling.DURATION" to durationV.text,
                        "com.teamriffle.carpooling.INTERMEDIATE_STOPS" to intermediateStopsV.text,
                        "com.teamriffle.carpooling.STATUS" to statusV.toString()
                ))
                true
            }
            else -> return super.onOptionsItemSelected(item)
        }
    }

    override fun onDestroyView() {
        super.onDestroyView()
        listener.remove()
    }
}