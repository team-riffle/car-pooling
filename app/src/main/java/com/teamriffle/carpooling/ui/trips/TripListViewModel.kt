package com.teamriffle.carpooling.ui.trips

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.google.firebase.firestore.FirebaseFirestore
import com.teamriffle.carpooling.entities.Booking
import com.teamriffle.carpooling.entities.Trip

class TripListViewModel : ViewModel() {

    private lateinit var trips: MutableList<Trip>

    init {
        trips = mutableListOf<Trip>()

    }



    //TODO: Replace with a Factory
    fun loadTrips(savedTrips: MutableList<Trip>){
        trips = savedTrips
    }

    fun getTrips(): MutableList<Trip> {
        return trips
    }

/*    fun addTrip(id: Int, *//*rider: String?,*//* departure: String?, arrival: String?, date: String?, duration: String?, seats: String?, price: String?, desc: String?, intermediateStops: String?, carUri: String?, other: Boolean){
        trips.add(Trip(id, *//*rider,*//* departure, arrival, date, duration, seats, price, desc, intermediateStops, carUri, other))
    }

    fun replaceTrip(id: Int, departure: String?, arrival: String?, date: String?, duration: String?, seats: String?, price: String?, desc: String?, intermediateStops: String?, carUri: String?, other: Boolean){
        val newTrip = trips.get(id)?.copy(departure = departure, arrival = arrival, date = date, duration = duration, seats = seats, price = price, desc = desc, intermediateStops = intermediateStops, carUri = carUri, other = other)
        trips.set(id, newTrip)
    }*/
}