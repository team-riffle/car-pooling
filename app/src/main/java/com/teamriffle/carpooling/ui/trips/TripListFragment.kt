package com.teamriffle.carpooling.ui.trips

import android.content.Context
import android.net.Uri
import android.os.Bundle
import android.view.*
import android.view.View.INVISIBLE
import android.view.View.VISIBLE
import android.widget.Button
import android.widget.ImageView
import android.widget.TextView
import androidx.cardview.widget.CardView
import androidx.core.os.bundleOf
import androidx.fragment.app.Fragment
import androidx.fragment.app.setFragmentResultListener
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.findNavController
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.google.android.gms.auth.api.signin.GoogleSignIn
import com.google.android.material.floatingactionbutton.FloatingActionButton
import com.google.android.material.snackbar.Snackbar
import com.google.firebase.firestore.FirebaseFirestore
import com.google.firebase.firestore.ListenerRegistration
import com.google.firebase.storage.FirebaseStorage
import com.teamriffle.carpooling.MainActivity
import com.teamriffle.carpooling.R
import com.teamriffle.carpooling.entities.Trip
import com.teamriffle.carpooling.entities.TripAdapter
import com.teamriffle.carpooling.entities.TripStatus
import java.util.ArrayList


class TripListFragment : Fragment() {
    private lateinit var listener: ListenerRegistration
    private lateinit var myContext: Context
    private lateinit var tripListViewModel: TripListViewModel
    private lateinit var tripAdapter: TripAdapter
    private lateinit var trips: MutableList<Trip>
    private lateinit var lastID: Number

    val db: FirebaseFirestore = FirebaseFirestore.getInstance()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        tripListViewModel =
            ViewModelProvider(this).get(TripListViewModel::class.java)

        if(container!=null) myContext = container.context

        return inflater.inflate(R.layout.fragment_trip_list, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val tripListRV : RecyclerView = view.findViewById(R.id.trip_list_rv)
        val initalMessage : TextView = view.findViewById(R.id.tripList_initialMessage)

        tripListRV.layoutManager = LinearLayoutManager(myContext)

        val userID = (activity as MainActivity).getEmail()

        trips = mutableListOf<Trip>()
        tripAdapter = TripAdapter(trips)

        lastID = -1

                val tripsRef = db.collection("users/$userID/trips")

                listener = tripsRef.addSnapshotListener { value, error ->
                    if (error != null) throw error
                    if (value != null) {
                        trips.clear()
                        for (trip in value) {
                            val tempStatus: TripStatus
                            if(trip["status"] != null) tempStatus = TripStatus.valueOf(trip["status"].toString())
                            else tempStatus = TripStatus.AVAILABLE
                            trips.add(
                                Trip(
                                    trip["id"].toString().toInt(),
                                    trip["departure"].toString(),
                                    trip["arrival"].toString(),
                                    trip["date"].toString(),
                                    trip["duration"].toString(),
                                    trip["seats"].toString(),
                                    trip["price"].toString(),
                                    trip["desc"].toString(),
                                    trip["intermediateStops"].toString(),
                                    trip["carUri"].toString(),
                                    false,
                                    trip["rider"].toString(),
                                    tempStatus)
                            )
                        }

                        if (trips.size == 0)
                            initalMessage.visibility = VISIBLE
                        else initalMessage.visibility = INVISIBLE

                        tripAdapter.setTrips(trips)

                        tripListRV.adapter = tripAdapter
                    }
                }




        setFragmentResultListener("requestKey") { _, bundle ->
            val action = bundle.getString("com.teamriffle.carpooling.ACTION")

            val id = bundle.getInt("com.teamriffle.carpooling.ID")
            //val riderID = bundle.getString("com.teamriffle.carpooling.RIDER")
            val departure = bundle.getString("com.teamriffle.carpooling.DEPARTURE")
            val arrival = bundle.getString("com.teamriffle.carpooling.ARRIVAL")
            val date = bundle.getString("com.teamriffle.carpooling.DATE")
            val duration = bundle.getString("com.teamriffle.carpooling.DURATION")
            val seats = bundle.getString("com.teamriffle.carpooling.SEATS")
            val price = bundle.getString("com.teamriffle.carpooling.PRICE")
            val desc = bundle.getString("com.teamriffle.carpooling.DESC")
            val intermediateStops = bundle.getString("com.teamriffle.carpooling.INTERMEDIATE_STOPS")
            val carUri = bundle.getString("com.teamriffle.carpooling.CAR_URI")
            val status = bundle.getString("com.teamriffle.carpooling.STATUS")

            val imgRef = FirebaseStorage.getInstance().getReference("/tripsImgs/$userID/$id")

            val db: FirebaseFirestore = FirebaseFirestore.getInstance()
//            db.collection("counter_trip").document("0").get().addOnSuccessListener {
//                value -> lastID = value["lastID"].toString().toInt()
                val counter_tripRef = db.collection("counter_trip")
                val tripRef = tripsRef.document("$id")
                imgRef.putFile(Uri.parse(carUri)).addOnSuccessListener {
                        println(id)
                    if(action == "Create") {
                        tripRef.set(mapOf(
                                "id" to id,
                                "rider" to userID,
                                "departure" to departure,
                                "arrival" to arrival,
                                "date" to date,
                                "duration" to duration,
                                "seats" to seats,
                                "price" to price,
                                "desc" to desc,
                                "intermediateStops" to intermediateStops,
                                "carUri" to carUri,
                                "bookedMap" to mapOf<String, Boolean>(),
                                "status" to TripStatus.AVAILABLE
                        ))

                        lastID = id as Int + 1
                        counter_tripRef.document("0").set(mapOf("lastID" to lastID))
                    }else{
                        tripRef.update(mapOf(
                                "id" to id,
                                "rider" to userID,
                                "departure" to departure,
                                "arrival" to arrival,
                                "date" to date,
                                "duration" to duration,
                                "seats" to seats,
                                "price" to price,
                                "desc" to desc,
                                "intermediateStops" to intermediateStops,
                                "carUri" to carUri,
                                "status" to status
                        ))
                    }
            }
        }

        val fab: FloatingActionButton = view.findViewById(R.id.fab)
        fab.setOnClickListener { view ->
            Snackbar.make(view, "Add a new trip advertisement", Snackbar.LENGTH_LONG)
                    .setAction("Action", null).show()
            db.collection("counter_trip").document("0").get().addOnSuccessListener { value ->
                lastID = value["lastID"].toString().toInt()
                view.findNavController().navigate(R.id.action_nav_tripList_to_nav_tripEdit, bundleOf(
                        "com.teamriffle.carpooling.ACTION" to "Create",
                        "com.teamriffle.carpooling.ID" to lastID,
                        "com.teamriffle.carpooling.RIDER" to userID,
                        "com.teamriffle.carpooling.DEPARTURE" to "Turin",
                        "com.teamriffle.carpooling.ARRIVAL" to "Roma",
                        "com.teamriffle.carpooling.DATE" to "2021-07-12",
                        "com.teamriffle.carpooling.SEATS" to "3",
                        "com.teamriffle.carpooling.PRICE" to "60",
                        "com.teamriffle.carpooling.DESC" to "Fill me!",
                        "com.teamriffle.carpooling.CAR_URI" to Uri.parse("android.resource://com.teamriffle.carpooling/" + R.drawable.ic_orange_sport_car).toString(),
                        "com.teamriffle.carpooling.DURATION" to "7h30m",
                        "com.teamriffle.carpooling.INTERMEDIATE_STOPS" to "Milano, Firenze",
                        "com.teamriffle.carpooling.STATUS" to TripStatus.AVAILABLE.toString()

                ))
            }
        }
    }

    override fun onResume() {

        super.onResume()
    }

    override fun onDestroyView() {
        super.onDestroyView()
        listener.remove()
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setHasOptionsMenu(true)
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        super.onCreateOptionsMenu(menu, inflater)
        menu.clear()
        inflater.inflate(R.menu.main, menu)
        var item =  menu.findItem(R.id.menu_search)
        item?.isVisible = false;
        item =  menu.findItem(R.id.tripDetailsEdit)
        item?.isVisible = false;
    }

}


