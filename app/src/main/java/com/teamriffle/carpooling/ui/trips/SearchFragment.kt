package com.teamriffle.carpooling.ui.trips

import android.content.Context
import android.net.Uri
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.EditText
import android.widget.Spinner
import androidx.cardview.widget.CardView
import androidx.core.os.bundleOf
import androidx.fragment.app.Fragment
import androidx.fragment.app.setFragmentResult
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.findNavController
import com.google.android.material.datepicker.MaterialDatePicker
import com.google.android.material.snackbar.Snackbar
import com.google.android.material.timepicker.MaterialTimePicker
import com.google.android.material.timepicker.TimeFormat
import com.teamriffle.carpooling.R
import com.teamriffle.carpooling.entities.Trip
import java.text.ParseException
import java.text.SimpleDateFormat
import java.util.*

class SearchFragment : Fragment() {

    private lateinit var myContext: Context
    private lateinit var searchButton: Button
    private lateinit var cancelButton: Button
    private lateinit var resetButton: Button
    private lateinit var searchDeparture: EditText
    private lateinit var searchArrival: EditText
    private lateinit var searchDate: EditText
    private lateinit var searchTime: EditText
    private lateinit var searchPrice: Spinner

    override fun onCreateView(
            inflater: LayoutInflater,
            container: ViewGroup?,
            savedInstanceState: Bundle?
    ): View? {
        if(container!=null) myContext = container.context
        return inflater.inflate(R.layout.search_fragment, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        searchButton = view.findViewById(R.id.searchButton)
        cancelButton = view.findViewById(R.id.cancelButton)
        resetButton = view.findViewById(R.id.resetButton)
        searchDeparture = view.findViewById(R.id.search_departure)
        searchArrival = view.findViewById(R.id.search_arrival)
        searchDate = view.findViewById(R.id.search_date)
        searchTime = view.findViewById(R.id.search_time)
        searchPrice = view.findViewById(R.id.search_price)

        searchDate.setOnClickListener{
            dateListener()
        }

        searchDate.setOnFocusChangeListener { _, hasFocus ->
            if(hasFocus)
                dateListener()
        }

        searchTime.setOnClickListener{
            timeListener()
        }

        searchTime.setOnFocusChangeListener { _, hasFocus ->
            if(hasFocus)
                timeListener()
        }

        searchButton.setOnClickListener {
            val departure = searchDeparture.text.toString()
            val arrival = searchArrival.text.toString()
            val date = searchDate.text.toString()
            val time = searchTime.text.toString()
            val price = searchPrice.selectedItemPosition
            //val df = SimpleDateFormat("yyyy/MM/dd")
            //df.isLenient = false
            //date validation
            var errors: Int = 0
            /*try {
                if(date.isNotEmpty())
                    df.parse(date)
            }catch (ex: ParseException){
                searchDate.error = "Invalid date or format, use yyyy/mm/dd"
                errors++
            }
            //time validation
            if(time.isNotEmpty() && !time.contains(":")){
                searchTime.error = "Invalid time or format, use HH:MM"
                errors++
            }
            else if(time.isNotEmpty()){
                val hh = time.split(":")[0].toInt()
                val mm = time.split(":")[1].toInt()
                if(hh < 0 || hh > 23 || mm < 0 || mm > 59){
                    searchTime.error = "Invalid format time, use HH:MM"
                    errors++
                }
            }*/
            if(departure.isEmpty() && arrival.isEmpty() && date.isEmpty() && time.isEmpty() && price==0){
                view.let {
                    Snackbar.make(it, "Please fill at least one field.", Snackbar.LENGTH_LONG)
                            .setAction("Action", null).show()
                }
                errors++
            }

            if(errors == 0){
                setFragmentResult("searchKey", bundleOf(
                        "com.teamriffle.carpooling.SEARCH_DEPARTURE" to departure,
                        "com.teamriffle.carpooling.SEARCH_ARRIVAL" to arrival,
                        "com.teamriffle.carpooling.SEARCH_DATE" to date,
                        "com.teamriffle.carpooling.SEARCH_TIME" to time,
                        "com.teamriffle.carpooling.SEARCH_PRICE" to price.toString()
                ))

                val navController = view.findNavController()
                navController.popBackStack()
            }
        }
        cancelButton.setOnClickListener {
            val navController = view.findNavController()
            navController.popBackStack()
        }
        resetButton.setOnClickListener {
            searchDeparture.setText("")
            searchArrival.setText("")
            searchDate.setText("")
            searchTime.setText("")
            searchPrice.setSelection(0)
        }
    }

    private fun dateListener(){
        val datePicker = MaterialDatePicker.Builder.datePicker()
            .setTitleText("Select a date")
            .build()

        datePicker.addOnPositiveButtonClickListener {
            val calendar = Calendar.getInstance(TimeZone.getTimeZone("UTC"))
            calendar.time = Date(it)
            val day = calendar.get(Calendar.DAY_OF_MONTH)
            val month = calendar.get(Calendar.MONTH) + 1
            val year = calendar.get(Calendar.YEAR)
            onDateSelected(day, month, year)
        }

        datePicker.show(parentFragmentManager, "search_datePicker")
    }

    private fun timeListener(){
        val timePicker = MaterialTimePicker.Builder()
            .setTimeFormat(TimeFormat.CLOCK_24H)
            .build()

        timePicker.addOnPositiveButtonClickListener {
            onTimeSelected(timePicker.hour, timePicker.minute)
        }

        timePicker.show(parentFragmentManager, "search_timePicker")
    }

    private fun onDateSelected(day: Int, month: Int, year: Int){
        searchDate.setText("$day/$month/$year")
        searchDate.clearFocus()
    }

    private fun onTimeSelected(hour: Int, minute: Int) {
        val hourAsText = if (hour < 10) "0$hour" else hour
        val minuteAsText = if (minute < 10) "0$minute" else minute

        searchTime.setText("$hourAsText:$minuteAsText")
        searchTime.clearFocus()
    }
}