package com.teamriffle.carpooling.ui.trips

import android.annotation.SuppressLint
import android.content.Context
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.util.Log
import android.view.*
import android.view.View.*
import android.widget.Button
import android.widget.TextView
import androidx.annotation.RequiresApi
import androidx.core.os.bundleOf
import androidx.fragment.app.Fragment
import androidx.fragment.app.setFragmentResultListener
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.android.material.floatingactionbutton.FloatingActionButton
import com.google.android.material.snackbar.Snackbar
import com.google.firebase.firestore.FirebaseFirestore
import com.google.firebase.firestore.ListenerRegistration
import com.teamriffle.carpooling.MainActivity
import com.teamriffle.carpooling.R
import com.teamriffle.carpooling.entities.Trip
import com.teamriffle.carpooling.entities.TripAdapter
import com.teamriffle.carpooling.entities.TripStatus
import java.text.SimpleDateFormat
import kotlin.math.max


class OthersTripListFragment : Fragment() {
    private lateinit var listener: ListenerRegistration
    private lateinit var myContext: Context
    private lateinit var othersTripListViewModel: OthersTripListViewModel
    private lateinit var tripAdapter: TripAdapter
    private lateinit var trips: MutableList<Trip>
    private var filtered: Boolean = false
    private lateinit var filteredTrips: MutableList<Trip>
    private lateinit var departure: String
    private lateinit var arrival: String
    private lateinit var date: String
    private lateinit var time: String
    private lateinit var price: String
    private lateinit var resetFilter: Button
    private lateinit var noResult: TextView
    private lateinit var tripListRV: RecyclerView

    val db: FirebaseFirestore = FirebaseFirestore.getInstance()

    //val users = db.collection("users").whereNotEqualTo("nickname", "JoJo")


    override fun onCreateView(
            inflater: LayoutInflater,
            container: ViewGroup?,
            savedInstanceState: Bundle?
    ): View? {
        othersTripListViewModel =
                ViewModelProvider(this).get(OthersTripListViewModel::class.java)

        if(container!=null) myContext = container.context

        //trips = tripsViewModel.getTrips()!!
        return inflater.inflate(R.layout.fragment_trip_list, container, false)
    }

    @RequiresApi(Build.VERSION_CODES.N)
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        tripListRV = view.findViewById(R.id.trip_list_rv)
        val initalMessage : TextView = view.findViewById(R.id.tripList_initialMessage)
        resetFilter = view.findViewById(R.id.resetFilter)
        noResult = view.findViewById(R.id.noResultsText)

        resetFilter.setOnClickListener {
            filtered = false
            tripAdapter.setTrips(trips)
            tripListRV.adapter = tripAdapter
            resetFilter.visibility = GONE
            if(noResult.visibility == VISIBLE) noResult.visibility = GONE
        }


        tripListRV.layoutManager = LinearLayoutManager(myContext)

        trips = mutableListOf<Trip>()/*othersTripListViewModel.getTrips()*/

        tripAdapter = TripAdapter(trips)

        val userID = (activity as MainActivity).getEmail()

        val tripsRef = db.collectionGroup("trips").whereNotEqualTo("rider", userID)

        listener = tripsRef.addSnapshotListener { value, error ->
            if (error != null) throw error
            if (value != null) {
                trips.clear()
                for (trip in value) {
                    val status = trip["status"].toString()
                    val bookedMap = trip["bookedMap"] as MutableMap<String,Boolean>

                    if (status != TripStatus.BLOCKED.toString() || bookedMap[userID] == true) {
                        trips.add(
                            Trip(
                                trip["id"].toString().toInt(),
                                trip["departure"].toString(),
                                trip["arrival"].toString(),
                                trip["date"].toString(),
                                trip["duration"].toString(),
                                trip["seats"].toString(),
                                trip["price"].toString(),
                                trip["desc"].toString(),
                                trip["intermediateStops"].toString(),
                                trip["carUri"].toString(),
                                true,
                                trip["rider"].toString(),
                                TripStatus.valueOf(trip["status"].toString())
                            )
                        )
                    }
                }

                        if (trips.size == 0)
                            initalMessage.visibility = VISIBLE
                        else initalMessage.visibility = INVISIBLE

                        if (filtered) {
                            applySearch(departure, arrival, date, time, price.toInt())
                            tripAdapter.setTrips(filteredTrips)
                        } else {
                            tripAdapter.setTrips(trips)
                        }

                        tripListRV.adapter = tripAdapter
                    }
                }


        /*users.get().addOnSuccessListener { snapshot -> snapshot.forEach{
            doc -> listener = doc.reference.collection("trips").addSnapshotListener{value, error ->
            if(error != null) throw error
            if(value != null){
                for(trip in value){println(trip["departure"].toString())
                    if(!trips.filter { it.id == trip["id"].toString().toInt()}.isEmpty())
                        othersTripListViewModel.replaceTrip(
                            trip["id"].toString().toInt(),
                            trip["departure"].toString(),
                            trip["arrival"].toString(),
                            trip["date"].toString(),
                            trip["duration"].toString(),
                            trip["seats"].toString(),
                            trip["price"].toString(),
                            trip["desc"].toString(),
                            trip["intermediateStops"].toString(),
                            trip["carUri"].toString(),
                            true,
                                trip["rider"].toString()
                        )
                    else
                        othersTripListViewModel.addTrip(
                            trip["id"].toString().toInt(),
                            trip["departure"].toString(),
                            trip["arrival"].toString(),
                            trip["date"].toString(),
                            trip["duration"].toString(),
                            trip["seats"].toString(),
                            trip["price"].toString(),
                            trip["desc"].toString(),
                            trip["intermediateStops"].toString(),
                            trip["carUri"].toString(),
                            true,
                                trip["rider"].toString()
                    )
                }

                trips = othersTripListViewModel.getTrips()

                if(trips.size == 0)
                    initalMessage.visibility = VISIBLE
                else initalMessage.visibility = INVISIBLE

                if(filtered){
                    applySearch(departure,arrival,date,time,price.toInt())
                    tripAdapter.setTrips(filteredTrips)
                } else{
                    tripAdapter.setTrips(trips)
                }

                tripListRV.adapter = tripAdapter
            }
        }
        listeners.add(listener)
        }}*/

        setFragmentResultListener("searchKey"){ _, bundle ->
            departure = bundle.getString("com.teamriffle.carpooling.SEARCH_DEPARTURE")?:""
            arrival = bundle.getString("com.teamriffle.carpooling.SEARCH_ARRIVAL")?:""
            date = bundle.getString("com.teamriffle.carpooling.SEARCH_DATE")?:""
            time = bundle.getString("com.teamriffle.carpooling.SEARCH_TIME")?:""
            price = bundle.getString("com.teamriffle.carpooling.SEARCH_PRICE")?:"0"
            filtered = true
            Log.d("testsearch", "frag result: $departure, $arrival, $date, $time, $price")

        }

        setFragmentResultListener("requestKey") { _, bundle ->
            val action = bundle.getString("com.teamriffle.carpooling.ACTION")

            val id = bundle.getInt("com.teamriffle.carpooling.ID")
            val rider = bundle.getString("com.teamriffle.carpooling.RIDER")
            val departure = bundle.getString("com.teamriffle.carpooling.DEPARTURE")
            val arrival = bundle.getString("com.teamriffle.carpooling.ARRIVAL")
            val date = bundle.getString("com.teamriffle.carpooling.DATE")
            val duration = bundle.getString("com.teamriffle.carpooling.DURATION")
            val seats = bundle.getString("com.teamriffle.carpooling.SEATS")
            val price = bundle.getString("com.teamriffle.carpooling.PRICE")
            val desc = bundle.getString("com.teamriffle.carpooling.DESC")
            val intermediateStops = bundle.getString("com.teamriffle.carpooling.INTERMEDIATE_STOPS")
            val carUri = bundle.getString("com.teamriffle.carpooling.CAR_URI")

        }

        val fab: FloatingActionButton = view.findViewById(R.id.fab)
        fab.visibility = INVISIBLE
//        fab.setOnClickListener {
//            view ->
//            Snackbar.make(view, "Add a new trip advertisement", Snackbar.LENGTH_LONG)
//                    .setAction("Action", null).show()
//            view.findNavController().navigate(R.id.action_nav_tripDetails_to_nav_tripEdit,bundleOf(
//                    "com.teamriffle.carpooling.ACTION" to "Create",
//                    "com.teamriffle.carpooling.ID" to trips.size,
//                    "com.teamriffle.carpooling.DEPARTURE" to "Turin",
//                    "com.teamriffle.carpooling.ARRIVAL" to "Roma",
//                    "com.teamriffle.carpooling.DATE" to "2021-07-12",
//                    "com.teamriffle.carpooling.SEATS" to "3",
//                    "com.teamriffle.carpooling.PRICE" to "60",
//                    "com.teamriffle.carpooling.DESC" to "Fill me!",
//                    "com.teamriffle.carpooling.CAR_URI" to Uri.parse("android.resource://com.teamriffle.carpooling/" + R.drawable.ic_orange_sport_car).toString(),
//                    "com.teamriffle.carpooling.DURATION" to "7h30m",
//                    "com.teamriffle.carpooling.INTERMEDIATE_STOPS" to "Milano, Firenze"))
//        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setHasOptionsMenu(true)
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        super.onCreateOptionsMenu(menu, inflater)
        menu.clear()
        inflater.inflate(R.menu.main, menu)
        val item =  menu.findItem(R.id.tripDetailsEdit)
        item?.isVisible = false;
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        // Handle item selection
        return when (item.itemId) {
            R.id.menu_search -> {
                view?.findNavController()?.navigate(R.id.action_nav_othersTripList_to_searchFragment)
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }

    override fun onResume() {

        super.onResume()
    }

    override fun onDestroyView() {
        super.onDestroyView()
        listener.remove()
    }

    @SuppressLint("SimpleDateFormat")
    private fun applySearch(departure: String,
                            arrival: String,
                            date: String,
                            time: String,
                            price: Int){
        var filteredList = trips
        val df = SimpleDateFormat("dd/MM/yyyy")
        if(departure.isNotEmpty()) filteredList = filteredList.filter { it.departure?.compareTo(departure,true)==0 }.toMutableList()
        if(arrival.isNotEmpty()) filteredList = filteredList.filter { it.arrival?.compareTo(arrival,true)==0 }.toMutableList()
        if(date.isNotEmpty()) filteredList = filteredList.filter {
            if(it.date == null)
                false
            else{
                val tripDate = it.date.split(" ")[0]
                df.parse(date) == df.parse(tripDate)
            }

        }.toMutableList()
        if(time.isNotEmpty()) filteredList = filteredList.filter {
            if(it.date == null || it.date.split(" ").size == 1)
                false
            else{
                val tripTime = it.date.split(" ")[1]
                val hour = time.split(":")[0].toInt()
                val minute = time.split(":")[1]
                var maxRange = "${if(hour+1 < 10)"0${hour+1}" else "${hour+1}"}:$minute"
                var minRange = "${if(hour-1 < 10)"0${hour-1}" else "${hour-1}"}:$minute"
                if(hour==23) maxRange="24:00"
                if(hour==0) minRange="00:00"
                Log.d("testsearch", "$time, $tripTime between $minRange and $maxRange")
                (tripTime in minRange..maxRange)
            }
        }.toMutableList()
        val count = resources.getStringArray(R.array.price_list).count()
        if(price!=0) filteredList = filteredList.filter {
            if(it.price==null) {
                false
            } else {
                when(price){
                    count-1 -> it.price.toInt() > 50*(price-1)
                    else -> it.price.toInt() > 50*(price-1) && it.price.toInt() < (50*price)-1
                }
            }
        }.toMutableList()
        Log.d("testsearch", "$departure, $arrival, $date, $time, $price")
        filteredTrips = filteredList
        resetFilter.visibility = VISIBLE
        if(filteredList.isEmpty()) noResult.visibility = VISIBLE
    }

    /*private fun initSearch(view: View){
        searchButton = view.findViewById(R.id.searchButton)
        cancelButton = view.findViewById(R.id.cancelButton)
        resetButton = view.findViewById(R.id.resetButton)
        searchDeparture = view.findViewById(R.id.search_departure)
        searchArrival = view.findViewById(R.id.search_arrival)
        searchDate = view.findViewById(R.id.search_date)
        searchTime = view.findViewById(R.id.search_time)
        searchPrice = view.findViewById(R.id.search_price)
        resetFilter = view.findViewById(R.id.resetFilter)
        noResult = view.findViewById(R.id.noResultsText)


        searchButton.setOnClickListener {
            val departure = searchDeparture.text.toString()
            val arrival = searchArrival.text.toString()
            val date = searchDate.text.toString().replace("/","-")
            val time = searchTime.text.toString()
            val price = searchPrice.selectedItemPosition
            var filteredList = trips
            val df = SimpleDateFormat("yyyy-MM-dd")
            df.isLenient = false
            //date validation
            var errors: Int = 0
            try {
                if(date.isNotEmpty())
                    df.parse(date)
            }catch (ex: ParseException){
                searchDate.error = "Invalid date or format, use yyyy/mm/dd"
                errors++
            }
            //time validation
            if(time.isNotEmpty() && !time.contains(":")){
                searchTime.error = "Invalid time or format, use HH:MM"
                errors++
            }
            else if(time.isNotEmpty()){
                val hh = time.split(":")[0].toInt()
                val mm = time.split(":")[1].toInt()
                if(hh < 0 || hh > 23 || mm < 0 || mm > 59){
                    searchTime.error = "Invalid format time, use HH:MM"
                    errors++
                }
            }

            if(departure.isEmpty() && arrival.isEmpty() && date.isEmpty() && time.isEmpty() && price==0){
                view.let {
                    Snackbar.make(it, "Please fill at least one field.", Snackbar.LENGTH_LONG)
                            .setAction("Action", null).show()
                }
                errors++
            }

            if(errors==0){
                if(departure.isNotEmpty()) filteredList = filteredList.filter { it.departure==departure }.toMutableList()
                if(arrival.isNotEmpty()) filteredList = filteredList.filter { it.arrival==arrival }.toMutableList()
                if(date.isNotEmpty()) filteredList = filteredList.filter {
                    if(it.date == null)
                        false
                    else
                        df.parse(date) == df.parse(it.date)
                }.toMutableList()
                if(price!=0) filteredList = filteredList.filter {
                    if(it.price==null) {
                        false
                    } else {
                        when(price){
                            searchPrice.count-1 -> it.price.toInt() > 50*(price-1)
                            else -> it.price.toInt() > 50*(price-1) && it.price.toInt() < 50*price-1
                        }
                    }
                }.toMutableList()
                //if(time.isNotEmpty()) filteredList = filteredList.filter { it.time = time }.toMutableList()
                //TODO add time in Trip class

                tripAdapter.setTrips(filteredList)
                tripListRV.adapter = tripAdapter
                searchView.visibility = GONE
                resetFilter.visibility = VISIBLE
                if(filteredList.isEmpty()) noResult.visibility = VISIBLE
            }
        }
        cancelButton.setOnClickListener { searchView.visibility = GONE }
        resetButton.setOnClickListener {
            searchDeparture.setText("")
            searchArrival.setText("")
            searchDate.setText("")
            searchTime.setText("")
            searchPrice.setSelection(0)
        }
        resetFilter.setOnClickListener {
            tripAdapter.setTrips(trips)
            tripListRV.adapter = tripAdapter
            resetFilter.visibility = GONE
            if(noResult.visibility == VISIBLE) noResult.visibility = GONE
        }

    }*/

}



