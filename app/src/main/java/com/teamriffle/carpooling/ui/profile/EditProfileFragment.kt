package com.teamriffle.carpooling.ui.profile

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.net.Uri
import android.os.Build
import androidx.lifecycle.ViewModelProvider
import android.os.Bundle
import android.text.TextUtils
import android.view.*
import androidx.fragment.app.Fragment
import android.widget.EditText
import android.widget.ImageButton
import android.widget.ImageView
import androidx.annotation.RequiresApi
import androidx.core.os.bundleOf
import androidx.fragment.app.setFragmentResult
import androidx.navigation.findNavController
import com.bumptech.glide.Glide
import com.google.android.material.snackbar.Snackbar
import com.google.firebase.storage.FirebaseStorage
import com.teamriffle.carpooling.MainActivity
import com.teamriffle.carpooling.R
import com.teamriffle.carpooling.entities.RequestCode
import com.teamriffle.carpooling.helpers.ImageInputHelper
import java.io.File

private var newPhoto: Boolean = false

class EditProfileFragment : Fragment() {

    companion object {
        fun newInstance() = EditProfileFragment()
    }

    private lateinit var viewModel: EditProfileViewModel
    private lateinit var myContext: Context

    private lateinit var fullName : EditText
    private lateinit var nickname : EditText
    private lateinit var email    : EditText
    private lateinit var location : EditText
    private lateinit var avatar_uri: Uri
    private lateinit var avatar   : ImageView

    var emailPattern = "[a-zA-Z0-9._-]+@[a-z]+\\.+[a-z]+"

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        if (container != null) {
            myContext = container.context
        }
        return inflater.inflate(R.layout.profile_edit_layout, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val userID = (activity as MainActivity).getEmail()
        val imgRef = FirebaseStorage.getInstance().getReference("/usersImgs/$userID")

        fullName = view.findViewById(R.id.editName)
        nickname = view.findViewById(R.id.editNickname)
        email = view.findViewById(R.id.editEmail)
        location = view.findViewById(R.id.editLocation)
        avatar = view.findViewById(R.id.editAvatar)

        fullName.setText(arguments?.getString("com.teamriffle.carpooling.FULLNAME"))
        nickname.setText(arguments?.getString("com.teamriffle.carpooling.NICKNAME"))
        email.setText(arguments?.getString("com.teamriffle.carpooling.EMAIL"))
        location.setText(arguments?.getString("com.teamriffle.carpooling.LOCATION"))
        avatar_uri = Uri.parse(arguments?.getString("com.teamriffle.carpooling.AVATAR_URI"))

        if(savedInstanceState != null) {
            avatar_uri = Uri.parse(savedInstanceState.getString("avatar"))
            avatar.setImageURI(avatar_uri)
        }else{
            imgRef.downloadUrl.addOnSuccessListener { url ->
                Glide.with(myContext)
                    .load(url)
                    .into(avatar)
            }
        }

        val cameraButton = view.findViewById<ImageButton>(R.id.cameraButton)
        registerForContextMenu(cameraButton)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        viewModel = ViewModelProvider(this).get(EditProfileViewModel::class.java)
        // TODO: Use the ViewModel
    }

    @RequiresApi(Build.VERSION_CODES.KITKAT)
    override fun onContextItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            R.id.galleryOption -> {
                ImageInputHelper.dispatchTakePictureFromGalleryIntent(myContext)
                true
            }
            R.id.cameraOption -> {
                ImageInputHelper.dispatchTakePictureFromCameraIntent(myContext) { uri ->
                    avatar_uri = uri
                }
                true
            }
            else -> super.onContextItemSelected(item)
        }
    }

    @RequiresApi(Build.VERSION_CODES.KITKAT)
    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        if (resultCode == Activity.RESULT_OK) {
            newPhoto = true
            if (requestCode == RequestCode.CAMERA_PROFILE) {
                avatar.setImageURI(avatar_uri)
                avatar_uri = Uri.fromFile(File(avatar_uri.path!!))
            }
            else if(requestCode == RequestCode.GALLERY_PROFILE){
                avatar_uri = data?.data!!
                avatar.setImageURI(avatar_uri)
                val takeFlags = Intent.FLAG_GRANT_READ_URI_PERMISSION
                (myContext as Activity).getContentResolver().takePersistableUriPermission(
                    avatar_uri,
                    takeFlags
                )
            }
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setHasOptionsMenu(true)
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        super.onCreateOptionsMenu(menu, inflater)
        menu.clear()
        inflater.inflate(R.menu.edit_option_menu, menu)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when(item.itemId){
            R.id.edit_save -> {
                var check = 0
                if(TextUtils.isEmpty(fullName.getText())) fullName.setError("Please Enter Your Full Name") else check++
                if(TextUtils.isEmpty(nickname.getText())) nickname.setError("Please Enter Your Nickname") else check++
                if(TextUtils.isEmpty(email.getText())) email.setError("Please Enter Your Email") else check++
                if(TextUtils.isEmpty(location.getText())) location.setError("Please Enter Your Location") else check++
                if (!email.text.toString().trim { it <= ' ' }.matches(emailPattern.toRegex())) email.setError("Please Enter A Valid Email") else check++

                if(check < 5) return false


                view?.let {
                    Snackbar.make(it, "Profile Saved", Snackbar.LENGTH_LONG)
                            .setAction("Action", null).show()
                }
                setFragmentResult("profileKey", bundleOf(
                    "com.teamriffle.carpooling.FULLNAME" to fullName.text.toString(),
                    "com.teamriffle.carpooling.NICKNAME" to nickname.text.toString(),
                    "com.teamriffle.carpooling.EMAIL" to email.text.toString(),
                    "com.teamriffle.carpooling.LOCATION" to location.text.toString(),
                    "com.teamriffle.carpooling.AVATAR_URI" to avatar_uri.toString()
                ))

                val navController = view?.findNavController()
                navController?.popBackStack()
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }

    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)

        if(newPhoto)
            outState.putString("avatar", avatar_uri.toString())
    }
}