package com.teamriffle.carpooling.ui.users

import androidx.lifecycle.ViewModel
import com.teamriffle.carpooling.entities.Trip
import com.teamriffle.carpooling.entities.User

class UsersViewModel : ViewModel() {
    private lateinit var users: MutableList<User>

    fun getUsers(): MutableList<User> {
        return users
    }

    fun getUser(username: String): User{
        return users.filter { it.nickname == username }[0]
    }

    fun loadUsers(){
    }
}