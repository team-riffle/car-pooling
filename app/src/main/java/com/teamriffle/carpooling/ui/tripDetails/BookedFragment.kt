package com.teamriffle.carpooling.ui.tripDetails

import android.content.Context
import android.content.res.ColorStateList
import android.graphics.Color
import android.graphics.Color.CYAN
import android.graphics.ColorSpace
import android.os.Build
import android.os.Bundle
import android.util.Log
import android.view.*
import android.view.View.GONE
import android.view.View.VISIBLE
import android.widget.*
import androidx.annotation.RequiresApi
import androidx.cardview.widget.CardView
import androidx.core.os.bundleOf
import androidx.fragment.app.Fragment
import androidx.navigation.findNavController
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.google.android.material.snackbar.Snackbar
import com.google.firebase.firestore.DocumentReference
import com.google.firebase.firestore.FieldValue
import com.google.firebase.firestore.FirebaseFirestore
import com.google.firebase.firestore.ListenerRegistration
import com.google.firebase.storage.FirebaseStorage
import com.google.protobuf.Empty
import com.teamriffle.carpooling.MainActivity
import com.teamriffle.carpooling.R
import com.teamriffle.carpooling.entities.*

private lateinit var tripRef: DocumentReference
private lateinit var bookedMap: MutableMap<String,Boolean>
private var seats: Int = 0

class BookedFragment : Fragment(){
    private lateinit var listener: ListenerRegistration
    private lateinit var myContext: Context


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        if (container != null) {
            myContext = container.context
        }
        return inflater.inflate(R.layout.fragment_booked, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val listView = view.findViewById<RecyclerView>(R.id.userList)
        val tripId = arguments?.getInt("com.teamriffle.carpooling.ID")!!
        val userId = (activity as MainActivity).getEmail() //TODO: userID da login

        val db: FirebaseFirestore = FirebaseFirestore.getInstance()
        val tripsRef = db.collection("users/$userId/trips")
        tripRef = tripsRef.document("$tripId")

        listView.layoutManager = LinearLayoutManager(myContext)
        /*listener = tripRef.addSnapshotListener{ value, error ->
            if(error != null) throw error
            if(value != null) {
                val bookedIds = value["booked"] as ArrayList<String>
                val users = db.collection("users")
                val booked = mutableMapOf<String, String>()
                var i = 0
                bookedIds.forEach{id ->
                    users.document(id).get().addOnSuccessListener { value ->
                        booked[value["nickname"].toString()] = value.id
                        if(i++ == bookedIds.size - 1){  //Ultima iterazione
                            val adapter = ArrayAdapter(myContext, android.R.layout.simple_list_item_1, booked.keys.toMutableList())
                            listView.adapter = adapter
                            listView.setOnItemClickListener { _, _, position, _ ->
                                val item = adapter.getItem(position)
                                view.findNavController().navigate(R.id.action_nav_tripBooked_to_nav_other_user_profile, bundleOf(
                                        "com.teamriffle.carpooling.PROFILE_MODE" to "OTHER_USER",
                                        "com.teamriffle.carpooling.RIDER" to booked[item]
                                ))
                            }
                        }
                    }
                }
            }
        }*/
        listener = tripRef.addSnapshotListener{ value, error ->
            if(error != null) throw error
            if(value != null){
                if(value["bookedMap"] != null)
                    bookedMap = value["bookedMap"] as MutableMap<String,Boolean>
                else
                    bookedMap = mutableMapOf()

                val users = db.collection("users")
                val bookedList: MutableList<Booked> = mutableListOf()
                seats = value["seats"].toString().toInt()
                Log.d("booked", "$bookedMap")

                if(bookedMap.isEmpty())
                    listView.adapter = null
                var i = 0
                bookedMap.forEach{ (id, status) ->
                    users.document(id).get().addOnSuccessListener {
                        bookedList.add(Booked(id,it["nickname"].toString(),status))
                        if(i++ == bookedMap.size - 1) {
                            listView.adapter = BookedAdapter(bookedList)
                        }
                    }
                }
            }
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setHasOptionsMenu(true)
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        super.onCreateOptionsMenu(menu, inflater)
        menu.clear()
    }

        override fun onDestroyView() {
        super.onDestroyView()
        listener.remove()
    }
}

class BookedAdapter(var users: List<Booked>): RecyclerView.Adapter<BookedAdapter.BookedViewHolder>() {
    class BookedViewHolder(v: View) : RecyclerView.ViewHolder(v) {
        val card = v.findViewById<CardView>(R.id.bookedCard)
        val nickname = v.findViewById<TextView>(R.id.bookedUser)
        val accept = v.findViewById<Button>(R.id.accept)
        val remove = v.findViewById<Button>(R.id.remove)
        fun bind(booked: Booked) {
            nickname.text = booked.nickname
            card.setOnClickListener{
                it.findNavController().navigate(R.id.action_nav_tripBooked_to_nav_other_user_profile, bundleOf(
                    "com.teamriffle.carpooling.PROFILE_MODE" to "OTHER_USER",
                    "com.teamriffle.carpooling.RIDER" to booked.id
                ))
            }
            if(booked.status){
                accept.visibility = GONE
                card.setBackgroundColor(CYAN)
            } else {
                accept.visibility = VISIBLE
                accept.setOnClickListener {
                    bookedMap[booked.id!!] = true
                    if(seats-- != 0){
                        tripRef.update(mapOf(
                            "bookedMap" to bookedMap,
                            "seats" to seats,
                            "status" to TripStatus.FULL
                        ))
                    } else {
                        it.let {
                            Snackbar.make(it, "There are no seats available!", Snackbar.LENGTH_LONG)
                                .setAction("Action", null).show()
                        }
                    }

                }
            }

            remove.setOnClickListener {
                bookedMap.remove(booked.id!!)
                if(booked.status){
                    if(seats++ == 0){
                        tripRef.update(mapOf(
                            "bookedMap" to bookedMap,
                            "seats" to seats,
                            "status" to TripStatus.AVAILABLE
                        ))
                    } else {
                        tripRef.update(mapOf(
                            "bookedMap" to bookedMap,
                            "seats" to seats
                        ))
                    }

                } else{
                    tripRef.update("bookedMap", bookedMap)
                }

            }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): BookedViewHolder {
        val layout = LayoutInflater.from(parent.context)
            .inflate(R.layout.booked_card, parent, false)
        return BookedViewHolder(layout)
    }

    override fun onBindViewHolder(holder: BookedViewHolder, position: Int) {
        holder.bind(users[position])
    }

    override fun getItemCount(): Int {
        return users.size
    }
}