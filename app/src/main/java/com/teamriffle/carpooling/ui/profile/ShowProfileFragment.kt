package com.teamriffle.carpooling.ui.profile

import android.content.Context
import android.net.Uri
import androidx.lifecycle.ViewModelProvider
import android.os.Bundle
import android.view.*
import androidx.fragment.app.Fragment
import android.widget.ImageView
import android.widget.TextView
import androidx.core.os.bundleOf
import androidx.fragment.app.setFragmentResultListener
import androidx.navigation.findNavController
import com.bumptech.glide.Glide
import com.google.firebase.firestore.DocumentReference
import com.google.firebase.firestore.FirebaseFirestore
import com.google.firebase.firestore.ListenerRegistration
import com.google.firebase.storage.FirebaseStorage
import com.google.firebase.storage.StorageException
import com.google.firebase.storage.StorageReference
import com.teamriffle.carpooling.MainActivity
import com.teamriffle.carpooling.R

class ShowProfileFragment : Fragment() {

    companion object {
        fun newInstance() = ShowProfileFragment()
    }

    private lateinit var viewModel: ShowProfileViewModel
    private lateinit var myContext: Context
    private lateinit var listener: ListenerRegistration

    private lateinit var fullName : TextView
    private lateinit var nickname : TextView
    private lateinit var email : TextView
    private lateinit var location : TextView
    private lateinit var avatar_uri   : Uri
    private lateinit var avatar : ImageView
    private lateinit var mode: String

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        if (container != null) {
            myContext = container.context
        }
        return inflater.inflate(R.layout.profile_layout, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val db: FirebaseFirestore = FirebaseFirestore.getInstance()
        val profile: DocumentReference
        val imgRef: StorageReference

        fullName = view.findViewById(R.id.name)
        nickname = view.findViewById(R.id.nickname)
        email = view.findViewById(R.id.email)
        location = view.findViewById(R.id.location)
        avatar = view.findViewById(R.id.avatar)
        avatar_uri = Uri.parse("android.resource://com.teamriffle.carpooling/" + R.drawable.ic_person_bounding_box)
        avatar.setImageURI(avatar_uri)

        mode = arguments?.getString("com.teamriffle.carpooling.PROFILE_MODE").toString()
        //mode = "OTHER_USER" // FOR TEST PURPOSE

        if(mode == "OTHER_USER"){
            email.visibility = View.GONE
            location.visibility = View.GONE

            val rider = arguments?.getString("com.teamriffle.carpooling.RIDER").toString()

            profile  = db.collection("users").document(rider)
            imgRef = FirebaseStorage.getInstance().getReference("/usersImgs/$rider")
        } else{
            val userID = (activity as MainActivity).getEmail()
            profile  = db.collection("users").document(userID)
            imgRef = FirebaseStorage.getInstance().getReference("/usersImgs/$userID")
        }

        listener = profile.addSnapshotListener { value, error ->
            if(error != null) throw error
            if(value != null){
                fullName.text = value["fullName"].toString()
                nickname.text = value["nickname"].toString()
                email.text = value["email"].toString()
                location.text = value["location"].toString()
                //avatar_uri = Uri.parse(value["avatar_uri"].toString())

                imgRef.downloadUrl.addOnSuccessListener { url ->
                    Glide.with(myContext)
                            .load(url)
                            .into(avatar)
                }
            }
        }

        setFragmentResultListener("profileKey"){ _, bundle ->
            val fullNameRes = bundle.getString("com.teamriffle.carpooling.FULLNAME")
            val nicknameRes = bundle.getString("com.teamriffle.carpooling.NICKNAME")
            val emailRes = bundle.getString("com.teamriffle.carpooling.EMAIL")
            val locationRes = bundle.getString("com.teamriffle.carpooling.LOCATION")
            val avatar_uriRes = bundle.getString("com.teamriffle.carpooling.AVATAR_URI")

            imgRef.putFile(Uri.parse(avatar_uriRes)).addOnSuccessListener {
                profile.set(mapOf(
                    "fullName" to fullNameRes,
                    "nickname" to nicknameRes,
                    "email" to emailRes,
                    "location" to locationRes,
                    "avatar_uri" to avatar_uriRes
                ))
            }
        }
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        viewModel = ViewModelProvider(this).get(ShowProfileViewModel::class.java)
        // TODO: Use the ViewModel
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setHasOptionsMenu(true)
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        super.onCreateOptionsMenu(menu, inflater)
        menu.clear()
        inflater.inflate(R.menu.profile_menu, menu)
        if(mode == "OTHER_USER"){
            val item = menu.findItem(R.id.edit_profile_menu_item)
            item?.isVisible = false
        }
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        // Handle item selection
        return when (item.itemId) {
            R.id.edit_profile_menu_item -> {
                view?.findNavController()?.navigate(R.id.action_nav_profile_to_nav_profileEdit, bundleOf(
                    "com.teamriffle.carpooling.FULLNAME" to fullName.text.toString(),
                    "com.teamriffle.carpooling.NICKNAME" to nickname.text.toString(),
                    "com.teamriffle.carpooling.EMAIL" to email.text.toString(),
                    "com.teamriffle.carpooling.LOCATION" to location.text.toString(),
                    "com.teamriffle.carpooling.AVATAR_URI" to avatar_uri.toString()
                ))
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }

    override fun onDestroyView() {
        super.onDestroyView()
        listener.remove()
    }
}