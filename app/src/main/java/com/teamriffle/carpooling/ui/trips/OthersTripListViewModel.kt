package com.teamriffle.carpooling.ui.trips

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.google.firebase.firestore.FirebaseFirestore
import com.teamriffle.carpooling.entities.Booking
import com.teamriffle.carpooling.entities.Trip
import com.teamriffle.carpooling.entities.TripStatus

class OthersTripListViewModel : ViewModel() {

    private lateinit var trips: MutableList<Trip>

    init {
        trips = mutableListOf<Trip>()

    }



    //TODO: Replace with a Factory
    fun loadTrips(savedTrips: MutableList<Trip>){
        trips = savedTrips
    }

    fun getTrips(): MutableList<Trip> {
        return trips
    }

    fun addTrip(id: Int, /*rider: String?,*/ departure: String?, arrival: String?, date: String?, duration: String?, seats: String?, price: String?, desc: String?, intermediateStops: String?, carUri: String?, other: Boolean, rider: String?, status: TripStatus){
        trips.add(Trip(id, /*rider,*/ departure, arrival, date, duration, seats, price, desc, intermediateStops, carUri, other, rider, status))
    }

    fun replaceTrip(id: Int, departure: String?, arrival: String?, date: String?, duration: String?, seats: String?, price: String?, desc: String?, intermediateStops: String?, carUri: String?, other: Boolean, rider: String?, status: TripStatus){
        val newTrip = trips.get(id)?.copy(departure = departure, arrival = arrival, date = date, duration = duration, seats = seats, price = price, desc = desc, intermediateStops = intermediateStops, carUri = carUri, other = other, rider = rider, status = status)
        trips.set(id, newTrip)
    }
}