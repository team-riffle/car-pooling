package com.teamriffle.carpooling.ui.bookings

import androidx.lifecycle.ViewModel
import com.teamriffle.carpooling.entities.Booking
import com.teamriffle.carpooling.entities.User

class BookingsViewModel : ViewModel() {
    private lateinit var bookings: MutableList<Booking>

    fun getBookings(): MutableList<Booking> {
        return bookings
    }

    fun getBooking(id: Int): Booking{
        return bookings.get(id)
    }

    fun loadBookings(){
    }

    fun addBooking(id: Int, rider: String, date: String, time: String, location: String, amount: Int, cell: Int, paymentStatus: String){
        bookings.add(Booking(id, rider, date, time, location, amount, cell, paymentStatus))
    }

    fun removeBooking(id: Int){
        bookings.removeAt(id)
    }
}