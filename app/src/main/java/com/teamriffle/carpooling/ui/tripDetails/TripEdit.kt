package com.teamriffle.carpooling.ui.tripDetails

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.text.TextUtils
import android.view.*
import android.view.inputmethod.InputMethodManager
import android.widget.*
import androidx.annotation.RequiresApi
import androidx.core.os.bundleOf
import androidx.fragment.app.Fragment
import androidx.fragment.app.setFragmentResult
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.findNavController
import com.bumptech.glide.Glide
import com.google.android.material.datepicker.MaterialDatePicker
import com.google.android.material.snackbar.Snackbar
import com.google.android.material.timepicker.MaterialTimePicker
import com.google.android.material.timepicker.TimeFormat
import com.google.firebase.firestore.DocumentReference
import com.google.firebase.firestore.FirebaseFirestore
import com.google.firebase.storage.FirebaseStorage
import com.teamriffle.carpooling.MainActivity
import com.teamriffle.carpooling.R
import com.teamriffle.carpooling.entities.RequestCode
import com.teamriffle.carpooling.entities.TripStatus
import com.teamriffle.carpooling.helpers.ImageInputHelper
import java.io.File
import java.util.*

private var newPhoto: Boolean = false

class TripEdit : Fragment() {
    private lateinit var action: String
    private lateinit var rider: String
    private lateinit var id: Number
    private lateinit var status: TripStatus
    private lateinit var departure: EditText
    private lateinit var arrival: EditText
    private lateinit var date: EditText
    private lateinit var durationH: NumberPicker
    private lateinit var durationM: NumberPicker
    private lateinit var seats: EditText
    private lateinit var price: EditText
    private lateinit var desc: EditText
    private lateinit var intermediateStops: EditText
    private lateinit var carPhoto: ImageView
    private lateinit var carUri: Uri
    private lateinit var tripRef: DocumentReference

    companion object {
        fun newInstance() = TripEdit()
    }

    private lateinit var viewModel: TripEditViewModel
    private lateinit var myContext: Context

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        if (container != null) {
            myContext = container.context
        }
        setHasOptionsMenu(true)
        return inflater.inflate(R.layout.fragment_trip_edit, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        viewModel = ViewModelProvider(this).get(TripEditViewModel::class.java)
        // TODO: Use the ViewModel
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        super.onCreateOptionsMenu(menu, inflater)
        menu.clear()
        inflater.inflate(R.menu.edit_trip_option_menu, menu)
        var item: MenuItem

        if(arguments?.getString("com.teamriffle.carpooling.ACTION").toString() == "Create"){
            item = menu.findItem(R.id.edit_trip_bookings)
            item?.setVisible(false)
            item = menu.findItem(R.id.edit_trip_block)
            item?.setVisible(false)
        }

        if(arguments?.getString("com.teamriffle.carpooling.STATUS").toString() == TripStatus.BLOCKED.toString()){
            item = menu.findItem(R.id.edit_trip_block)
            item.title = "Unlock"
        }

        }

    @RequiresApi(Build.VERSION_CODES.O)
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        departure = view.findViewById(R.id.tripEdit_departure_value)
        arrival = view.findViewById(R.id.tripEdit_arrival_value)
        date = view.findViewById(R.id.tripEdit_date_value)
        durationH = view.findViewById(R.id.tripEdit_duration_valueH)
        durationM = view.findViewById(R.id.tripEdit_duration_valueM)
        seats = view.findViewById(R.id.tripEdit_seats_value)
        price = view.findViewById(R.id.tripEdit_price_value)
        desc = view.findViewById(R.id.tripEdit_desc_value)
        intermediateStops = view.findViewById(R.id.tripEdit_intermediateStops_value)
        carPhoto = view.findViewById(R.id.tripEdit_carPhoto)
        val editCarPhoto = view.findViewById<ImageButton>(R.id.tripEdit_cameraButton)
        registerForContextMenu(editCarPhoto)

        val userID = (activity as MainActivity).getEmail()

        action = arguments?.getString("com.teamriffle.carpooling.ACTION").toString()

        id = arguments?.getInt("com.teamriffle.carpooling.ID")!!
        rider = arguments?.getString("com.teamriffle.carpooling.RIDER").toString()
        departure.setText(arguments?.getString("com.teamriffle.carpooling.DEPARTURE"))
        arrival.setText(arguments?.getString("com.teamriffle.carpooling.ARRIVAL"))
        date.setText(arguments?.getString("com.teamriffle.carpooling.DATE"))
        price.setText(arguments?.getString("com.teamriffle.carpooling.PRICE"))
        seats.setText(arguments?.getString("com.teamriffle.carpooling.SEATS"))
        desc.setText(arguments?.getString("com.teamriffle.carpooling.DESC"))
        carUri = Uri.parse(arguments?.getString("com.teamriffle.carpooling.CAR_URI"))
        val durationFromSharedPref = arguments?.getString("com.teamriffle.carpooling.DURATION")
        intermediateStops.setText(arguments?.getString("com.teamriffle.carpooling.INTERMEDIATE_STOPS"))
        val tempStatus = arguments?.getString("com.teamriffle.carpooling.STATUS")
        if(!tempStatus.isNullOrEmpty()) status = TripStatus.valueOf(tempStatus) else status = TripStatus.AVAILABLE

        val db: FirebaseFirestore = FirebaseFirestore.getInstance()
        val tripsRef = db.collection("users/$userID/trips")
        tripRef = tripsRef.document("$id")

        durationH.minValue = 0
        durationH.maxValue = 50

        durationM.minValue = 0
        durationM.maxValue = 59

        if(durationFromSharedPref!=null){
            val duration = Regex("""[hHmM]""").split(durationFromSharedPref)

            durationH.value = if(!duration[0].isEmpty()) duration[0].toInt() else 0
            durationM.value = if(!duration[1].isEmpty()) duration[1].toInt() else 0
        }

        if (savedInstanceState != null) {
            val H = savedInstanceState.getInt("durationH")
            durationH.value = H

            val M = savedInstanceState.getInt("durationM")
            durationM.value = M

            if(newPhoto) {
                carUri = Uri.parse(savedInstanceState.getString("carUri"))
                carPhoto.setImageURI(carUri)
            }
        }else{
            val imgRef = FirebaseStorage.getInstance().getReference("/tripsImgs/$userID/$id")
            imgRef.downloadUrl.addOnSuccessListener { url ->
                Glide.with(myContext)
                    .load(url)
                    .into(carPhoto)
            }
        }

        date.setOnClickListener{
            dateListener()
        }

        date.setOnFocusChangeListener { _, hasFocus ->
            if(hasFocus)
                dateListener()
        }
    }

    private fun dateListener(){
        val datePicker = MaterialDatePicker.Builder.datePicker()
                .setTitleText("Select a date")
                .build()

        datePicker.addOnPositiveButtonClickListener {
            val calendar = Calendar.getInstance(TimeZone.getTimeZone("UTC"))
            calendar.time = Date(it)
            val day = calendar.get(Calendar.DAY_OF_MONTH)
            val month = calendar.get(Calendar.MONTH) + 1
            val year = calendar.get(Calendar.YEAR)

            val timePicker = MaterialTimePicker.Builder()
                    .setTimeFormat(TimeFormat.CLOCK_24H)
                    .build()

            timePicker.addOnPositiveButtonClickListener {
                onTimeSelected(day, month, year, timePicker.hour, timePicker.minute)
            }

            timePicker.show(parentFragmentManager, "tripEdit_timePicker")
        }

        datePicker.show(parentFragmentManager, "tripEdit_datePicker")
    }

    private fun onTimeSelected(day: Int, month: Int, year: Int, hour: Int, minute: Int) {
        val hourAsText = if (hour < 10) "0$hour" else hour
        val minuteAsText = if (minute < 10) "0$minute" else minute

        date.setText("$day/$month/$year $hourAsText:$minuteAsText")
        date.clearFocus()

    }

    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)

        outState.putInt("durationH", durationH.value)
        outState.putInt("durationM", durationM.value)

        if(newPhoto)
            outState.putString("carUri", carUri.toString())
    }

    @RequiresApi(Build.VERSION_CODES.KITKAT)
    override fun onContextItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            R.id.galleryOption -> {
                ImageInputHelper.dispatchTakePictureFromGalleryIntent(myContext)
                true
            }
            R.id.cameraOption -> {
                ImageInputHelper.dispatchTakePictureFromCameraIntent(myContext) { uri ->
                    carUri = uri
                }
                true
            }
            else -> super.onContextItemSelected(item)
        }
    }

    @RequiresApi(Build.VERSION_CODES.KITKAT)
    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        if (resultCode == Activity.RESULT_OK) {
            newPhoto = true
            if (requestCode == RequestCode.CAMERA_PROFILE) {
                carPhoto.setImageURI(carUri)
                carUri = Uri.fromFile(File(carUri.path!!))
            }
            else if(requestCode == RequestCode.GALLERY_PROFILE){
                carUri = data?.data!!
                carPhoto.setImageURI(carUri)
                val takeFlags = Intent.FLAG_GRANT_READ_URI_PERMISSION
                (myContext as Activity).getContentResolver().takePersistableUriPermission(
                    carUri,
                    takeFlags
                )
            }
        }
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when(item.itemId){
            R.id.edit_trip_save -> {
                val imm = activity?.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
                imm.hideSoftInputFromWindow(requireView().getWindowToken(), 0)

                var check = 0
                if (TextUtils.isEmpty(departure.getText())) departure.setError("Please enter the departure") else check++
                if (TextUtils.isEmpty(arrival.getText())) arrival.setError("Please enter the arrival") else check++
                if (TextUtils.isEmpty(date.getText())) date.error else check++
                if (TextUtils.isEmpty(seats.getText())) seats.setError("Please enter the seats") else check++
                if (TextUtils.isEmpty(price.getText())) price.setError("Please enter the price") else check++
                if (TextUtils.isEmpty(desc.getText())) desc.setError("Please enter a description") else check++

                if (check < 6) return false

                view?.let {
                    Snackbar.make(it, "Trip Saved", Snackbar.LENGTH_LONG)
                            .setAction("Action", null).show()
                }
                val duration = durationH.value.toString() + "h" + durationM.value.toString() + "m"
                setFragmentResult("requestKey", bundleOf(
                   "com.teamriffle.carpooling.ACTION" to action,
                   "com.teamriffle.carpooling.ID" to id,
                    "com.teamriffle.carpooling.DEPARTURE" to departure.text.toString(),
                    "com.teamriffle.carpooling.ARRIVAL" to arrival.text.toString(),
                    "com.teamriffle.carpooling.DATE" to date.text.toString(),
                    "com.teamriffle.carpooling.SEATS" to seats.text.toString(),
                    "com.teamriffle.carpooling.PRICE" to price.text.toString(),
                    "com.teamriffle.carpooling.DESC" to desc.text.toString(),
                    "com.teamriffle.carpooling.CAR_URI" to carUri.toString(),
                    "com.teamriffle.carpooling.DURATION" to duration,
                    "com.teamriffle.carpooling.INTERMEDIATE_STOPS" to intermediateStops.text.toString(),
                    "com.teamriffle.carpooling.NEWPHOTO" to newPhoto,
                    "com.teamriffle.carpooling.STATUS" to status.toString()
                ))
                val navController = view?.findNavController()
                navController?.popBackStack()
                true
            }
            R.id.edit_trip_bookings -> {
                view?.findNavController()!!
                    .navigate(R.id.action_nav_tripEdit_to_nav_tripBooked, bundleOf(
                        "com.teamriffle.carpooling.ID" to id
                    ))
                true
            }
            R.id.edit_trip_block -> {
                val newStatus: TripStatus
                var message: String  = "Trip "
                if(status == TripStatus.AVAILABLE) {
                    newStatus = TripStatus.BLOCKED
                    message+= " Blocked"
                } else {
                    newStatus = TripStatus.AVAILABLE
                    message+= " Unlocked"
                }
                tripRef.update(mapOf(
                    "status" to newStatus
                ))
                view?.let {
                    Snackbar.make(it, message, Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show()
                }

                val duration = durationH.value.toString() + "h" + durationM.value.toString() + "m"
                setFragmentResult("requestKey", bundleOf(
                    "com.teamriffle.carpooling.ACTION" to action,
                    "com.teamriffle.carpooling.ID" to id,
                    "com.teamriffle.carpooling.DEPARTURE" to departure.text.toString(),
                    "com.teamriffle.carpooling.ARRIVAL" to arrival.text.toString(),
                    "com.teamriffle.carpooling.DATE" to date.text.toString(),
                    "com.teamriffle.carpooling.SEATS" to seats.text.toString(),
                    "com.teamriffle.carpooling.PRICE" to price.text.toString(),
                    "com.teamriffle.carpooling.DESC" to desc.text.toString(),
                    "com.teamriffle.carpooling.CAR_URI" to carUri.toString(),
                    "com.teamriffle.carpooling.DURATION" to duration,
                    "com.teamriffle.carpooling.INTERMEDIATE_STOPS" to intermediateStops.text.toString(),
                    "com.teamriffle.carpooling.NEWPHOTO" to newPhoto,
                    "com.teamriffle.carpooling.STATUS" to newStatus.toString()
                ))
                val navController = view?.findNavController()
                navController?.popBackStack()
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }
}