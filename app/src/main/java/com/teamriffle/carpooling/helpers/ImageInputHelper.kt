package com.teamriffle.carpooling.helpers

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.net.Uri
import android.os.Build
import android.os.Environment
import android.provider.MediaStore
import androidx.annotation.RequiresApi
import androidx.core.content.FileProvider
import com.teamriffle.carpooling.entities.RequestCode
import java.io.File
import java.io.IOException
import java.text.SimpleDateFormat
import java.util.*

object ImageInputHelper {
    /**
     * Takes picture from Camera
     * @param context
     * @return
     */
    fun dispatchTakePictureFromCameraIntent(c: Context,setUri: (Uri) -> (Unit)) {
        Intent(MediaStore.ACTION_IMAGE_CAPTURE).also { takePictureIntent ->
            // Create the File where the photo should go
            val photoFile: File? = try {
                createImageFile(c).apply {
                    // Save a file: path for use with ACTION_VIEW intents
                    setUri(Uri.parse(absolutePath))
                }
            } catch (ex: IOException) {
                // Error occurred while creating the File
                println(ex.message)
                null
            }
            // Continue only if the File was successfully created
            photoFile?.also {
                val photoURI: Uri = FileProvider.getUriForFile(
                    c,
                    "com.teamriffle.carpooling.fileprovider",
                    it
                )
                takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, photoURI)
                (c as Activity).startActivityForResult(takePictureIntent, RequestCode.CAMERA_PROFILE)
            }
        }
    }

    /**
     * Takes picture from Gallery
     * @param context
     * @return
     */
    @RequiresApi(Build.VERSION_CODES.KITKAT)
    fun dispatchTakePictureFromGalleryIntent(c: Context) {
        val intent = Intent(Intent.ACTION_OPEN_DOCUMENT)
        intent.type = "image/*"
        (c as Activity).startActivityForResult(intent, RequestCode.GALLERY_PROFILE)
    }

    @Throws(IOException::class)
    private fun createImageFile(c: Context): File {
        // Create an image file name
        val timeStamp: String = SimpleDateFormat("yyyyMMdd_HHmmss", Locale.ITALY).format(Date())
        val storageDir: File? = c.getExternalFilesDir(Environment.DIRECTORY_PICTURES)
        return File.createTempFile(
            "JPEG_${timeStamp}_", /* prefix */
            ".jpg", /* suffix */
            storageDir /* directory */
        )
    }
}