package com.teamriffle.carpooling

import android.content.Context
import android.content.Intent
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.view.ContextMenu
import android.view.Menu
import android.view.MenuInflater
import android.view.View
import android.widget.ImageView
import android.widget.TextView
import androidx.annotation.RequiresApi
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import androidx.drawerlayout.widget.DrawerLayout
import androidx.navigation.findNavController
import androidx.navigation.ui.AppBarConfiguration
import androidx.navigation.ui.navigateUp
import androidx.navigation.ui.setupActionBarWithNavController
import androidx.navigation.ui.setupWithNavController
import com.google.android.gms.auth.api.signin.GoogleSignIn
import com.google.android.material.navigation.NavigationView
import com.squareup.picasso.Picasso
import com.teamriffle.carpooling.entities.User
import kotlinx.serialization.decodeFromString
import kotlinx.serialization.json.Json


class MainActivity : AppCompatActivity() {

    private lateinit var appBarConfiguration: AppBarConfiguration
    private lateinit var navName: TextView
    private lateinit var navEmail: TextView
    private lateinit var avatar: ImageView

    private var personName: String = ""
    private var personEmail: String = ""
    private var personPhoto: String = ""


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        val toolbar: Toolbar = findViewById(R.id.toolbar)
        setSupportActionBar(toolbar)

        val drawerLayout: DrawerLayout = findViewById(R.id.drawer_layout)
        val navView: NavigationView = findViewById(R.id.nav_view)
        val navController = findNavController(R.id.nav_host_fragment)
        // Passing each menu ID as a set of Ids because each
        // menu should be considered as top level destinations.
        appBarConfiguration = AppBarConfiguration(
            setOf(
                R.id.nav_profile, R.id.nav_othersTripList, R.id.nav_tripList
            ), drawerLayout
        )
        setupActionBarWithNavController(navController, appBarConfiguration)
        navView.setupWithNavController(navController)

        navController.addOnDestinationChangedListener { controller, destination, arguments ->
            title = when (destination.id) {
                R.id.nav_othersTripList -> "Trip Advertisements"
                R.id.nav_tripList -> "My Trip Advertisements"
                R.id.nav_profile -> "Profile"
                R.id.nav_profileEdit -> "Edit Profile"
                R.id.nav_tripDetails -> "Trip Details"
                R.id.nav_tripEdit -> "Edit Trip"
                R.id.nav_tripBooked -> "Users Booked"
                R.id.nav_other_user_profile -> "Other User Profile"
                else -> "Default title"
            }

            supportActionBar?.title = title
        }

        val headerView = navView.getHeaderView(0)
        navName = headerView.findViewById<TextView>(R.id.nav_name)
        navEmail = headerView.findViewById<TextView>(R.id.nav_email)
        avatar = headerView.findViewById<ImageView>(R.id.imageView)
        setDrawerProfileInfo()

        val acct = GoogleSignIn.getLastSignedInAccount(this)
        if (acct != null) {
            personName = acct.displayName.toString()
//            personGivenName = acct.givenName
//            personFamilyName = acct.familyName
            personEmail = acct.email.toString()
//            personId = acct.id
            personPhoto = acct.photoUrl.toString()
        }
    }

    private fun setDrawerProfileInfo() {
        val sharedPref = getSharedPreferences(
            "com.teamriffle.carpooling.PROFILE_PREFERENCE",
            Context.MODE_PRIVATE
        )

        navName.text = personName
        navEmail.text = personEmail
        if (personPhoto.isNotEmpty())
            Picasso.get().load(personPhoto).into(avatar);

    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        // Inflate the menu; this adds items to the action bar if it is present.
        menuInflater.inflate(R.menu.main, menu)
        val item = menu.findItem(R.id.tripDetailsEdit)
        item?.setVisible(false);
        return true
    }

    override fun onSupportNavigateUp(): Boolean {
        setDrawerProfileInfo()
        val navController = findNavController(R.id.nav_host_fragment)
        return navController.navigateUp(appBarConfiguration) || super.onSupportNavigateUp()
    }

    override fun onCreateContextMenu(
        menu: ContextMenu?,
        v: View?,
        menuInfo: ContextMenu.ContextMenuInfo?
    ) {
        super.onCreateContextMenu(menu, v, menuInfo)
        val inflater: MenuInflater = menuInflater
        inflater.inflate(R.menu.avatar_options_menu, menu)
    }

    @RequiresApi(Build.VERSION_CODES.KITKAT)
    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        val navHostFragment = supportFragmentManager.findFragmentById(R.id.nav_host_fragment)
        val childFragments = navHostFragment?.childFragmentManager?.fragments
        childFragments?.forEach { it.onActivityResult(requestCode, resultCode, data) }
    }

    fun getUsername(): String {

        val acct = GoogleSignIn.getLastSignedInAccount(this)
        if (acct != null) {
            personName = acct.displayName.toString()
//            personId = acct.id
        }
        return personName
    }

    fun getEmail(): String {

        val acct = GoogleSignIn.getLastSignedInAccount(this)
        if (acct != null) {
            personEmail = acct.email.toString()
//            personId = acct.id
        }
        return personEmail
    }

}